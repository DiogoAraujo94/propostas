import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PropostaComponent} from './proposta/proposta.component';
import {LoginComponent} from './login/login.component';

const routes: Routes = [
  { path: ':id', component: LoginComponent },
  { path: ':id/proposal', component: PropostaComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      anchorScrolling: 'enabled'
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
