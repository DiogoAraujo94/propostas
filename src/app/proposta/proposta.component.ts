import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';
import {OwlOptions} from 'ngx-owl-carousel-o';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import {Title} from '@angular/platform-browser';
import {Proposal, ProposalsService} from '../shared/proposals.service';
import {Doctor, DoctorsService} from '../shared/doctors.service';

@Component({
    selector: 'app-proposta',
    templateUrl: './proposta.component.html',
    styleUrls: ['./proposta.component.css'],
})
class PropostaComponent implements OnInit {
    orcamento = false;
    firstPrest = 0;
    firstPrestBool = false;
    secondPrest = 0;
    secondPrestBool = false;
    thirdPrest = 0;
    thirdPrestBool = false;
    forthPrest = 0;
    forthPrestBool = false;
    prontoPagamento = 0;
    prestaPagamento = 0;
    nome = 'Paciente Desconhecido';
    antes = 'assets/images/antes.png';
    depois = 'assets/images/profile.png';
    depoisPerfil = 'assets/images/profile.png';
    comercialNome = 'Fábio Martins';
    comercialFoto = 'assets/images/comercial.png';
    comercialTelefone = '+351 912 345 556';
    comercialEmail = 'Martins.fabio@angelsmile.com.pt';
    clinica = '';
    medicos: Doctor[];
    especialidades = [];
    someDate = new Date();
    dateFormated = this.someDate.toISOString().substr(0, 10);
    mensalidade = '';
    taeg = '';
    dateaux = '';
    counts = {};
    dentes = {};
    finalProds : any[] = [];


    customOptions: OwlOptions = {
        loop: true,
        mouseDrag: true,
        touchDrag: true,
        pullDrag: true,
        center: true,
        autoplay: true,
        autoplaySpeed: 800,
        dots: true,
        navSpeed: 800,
        // navText: ['', ''],
        items: 1,
        // nav: true
    };
    slidesStore: any[] = [
        {
            id: 1,
            src: 'assets/images/profile.png',
            alt: '"É daqueles sítios em que não nos sentimos apenas mais um, porque toda a gente nos conhece. Nunca conheci uma equipa de trabalho que se tratasse como família, como aqui na AngelSmile. Sempre fui muito bem acolhida e sempre foram todos muito atenciosos comigo"',
            title: 'Ana Rita Pereira, Barcelos'
        },
        {
            id: 2,
            src: 'assets/images/daniel.jpeg',
            alt: '"Em relação ao tratamento na AngelSmile, o que mais se nota é a simpatia que têm com todos os pacientes. São super simpáticos. Acho que é isso que marca a diferença."',
            title: 'Daniel Fernando, Barcelos'
        },
        {
            id: 3,
            src: 'assets/images/maria_jose.png',
            alt: '"A clínica é super boa. Todas as pessoas são muito simpáticas. Não podia ser melhor. O resultado superou as minhas expectativas. Foi fantástico. Agora, vou ter uma qualidade de vida boa."',
            title: 'Maria José Ramalho, Barcelos'
        },
        {
            id: 4,
            src: 'assets/images/paulo.jpeg',
            alt: '"Tinha complexos com a outra prótese que eu tinha, que era removível. Eu falava, ela saía e escorregava. Não conseguia comer bem. Mesmo a nível social, tentava sempre esconder isso. Não sei se haverá palavras para explicar o que sente quando vemos o resultado final. A minha autoestima subiu em flecha. Foi uma experiência fantástica."',
            title: 'Paulo Bernardino, Faro'
        },
    ];
    clinicaImagem: any;
    clinicaTexto: any;
    consultas: any[];
    consultasGroups: any[];
    casosVideos = [
        {
            placeholder: 'assets/images/maria_jose.png',
            youtubeVideoId: 'evOX3CTP7sM'
        },
        {
            placeholder: 'assets/images/paulo_bernardino.png',
            youtubeVideoId: 'KulY-jix5hY'
        }
    ];

    prods: any;
    private proposalId: any;
    proposal: Proposal;
    private cadeira: any;
    private metodos: any[];
    private clinicaCardImage1: any;
    private clinicaCardText1: any;
    private clinicaCardImage2: any;
    private clinicaCardText2: any;
    private clinicaCardImage3: any;
    private clinicaCardText3: any;
    public dataFinal: string;
    isGaranteeNecessary = false;


    constructor(
        private httpClient: HttpClient,
        private route: ActivatedRoute,
        private router: Router,
        private titleService: Title,
        private dialog: MatDialog,
        private proposalsService: ProposalsService,
        private doctorsService: DoctorsService,
    ) {
    }


    async ngOnInit(): Promise<void> {
        this.proposalId = this.route.snapshot.paramMap.get('id');

        if (!this.proposalsService.isAuthenticated()) {
            await this.router.navigate([this.proposalId]);
        } else {
            this.route.fragment.subscribe(f => {
                const element = document.querySelector('#' + f);
                if (element) {
                    element.scrollIntoView();
                }
            });

            this.proposal = this.proposalsService.getProposal();

            this.nome = this.proposal.pacienteNome || this.nome;
            this.titleService.setTitle(this.nome + ' - Proposta Angelsmile');

            this.prods = JSON.parse(this.proposal.produtos);
            this.prods.forEach(prod => {
                if (prod.nome === 'Cirurgia para colocação de implantes com provisionalização removível imediata de arcada total'
                    || prod.nome === 'Cirurgia para colocação de implantes com provisionalização fixa imediata de arcada total')
                    this.isGaranteeNecessary = true;
            });
            console.log('garantia: ',this.isGaranteeNecessary);
            this.clinica = this.proposal.clinicaNome || '';
            this.cadeira = this.proposal.cadeira || 6;
            this.someDate.setDate(this.someDate.getDate() + (this.cadeira * 30));
            this.dateFormated = this.someDate.toISOString().substr(0, 10);
            const option = {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
            }

            var counts = [];
            var dentes = [];
            this.prods.forEach(function (x) {
                counts[x.nome] = (counts[x.nome] || 0) + 1;
                dentes[x.nome] = dentes[x.nome] ? dentes[x.nome] + (x.dente + ' | ') : (x.dente + ' | ');
            });

            this.dentes = dentes;
            this.counts = counts;

            for(var obj in this.counts){
                this.finalProds.push({nome: obj, count: this.counts[obj], dentes: this.dentes[obj].slice(0,this.dentes[obj].length-3)})
            }
            this.dataFinal = this.someDate.toLocaleDateString('pt-pt', option);
            this.firstPrest = Math.floor(this.proposal.valor / 12);
            this.secondPrest = Math.floor(this.proposal.valor / 24);
            this.thirdPrest = Math.floor(this.proposal.valor / 36);
            this.forthPrest = Math.floor((this.proposal.valor + this.proposal.valor * 0.1) / 48);
            this.prontoPagamento = this.proposal.descontoTotal ? this.proposal.valor - this.proposal.descontoTotal : null;
            this.prestaPagamento = this.proposal.valor;
            this.comercialEmail = this.proposal.useremail;
            this.comercialTelefone = this.proposal.usertele;
            this.comercialNome = this.proposal.usernome;
            this.comercialFoto = this.proposal.userfoto;
            this.antes = this.proposal.antes !== '' ? this.proposal.antes : this.antes;
            this.depois = this.proposal.depois !== '' ? this.proposal.depois : this.depois;
            this.depoisPerfil = this.proposal.depois !== '' ? this.proposal.depois : undefined;
            this.clinicaImagem = this.chooseClinicImg(this.clinica);
            this.consultas = JSON.parse(this.proposal.consultas);
            this.mensalidade = this.proposal.mensalidade ? this.proposal.mensalidade : '';
            this.taeg = this.proposal.taeg ? this.proposal.taeg : '';
            this.metodos = JSON.parse(this.proposal.metodos)
            if (this.metodos) {
                this.metodos.forEach(method => {
                    if (method.includes('12x')) {
                        this.firstPrestBool = true;
                    }
                    if (method.includes('24x')) {
                        this.secondPrestBool = true;
                    }
                    if (method.includes('36x')) {
                        this.thirdPrestBool = true;
                    }
                    if (method.includes('48x')) {
                        this.forthPrestBool = true;
                    }
                });
            }

            this.consultasGroups = [];

            this.consultas.forEach((consulta, index) => {
                const groupIndex = Math.floor(index / 3);
                if (!this.consultasGroups[groupIndex]) {
                    this.consultasGroups[groupIndex] = [];
                }
                this.consultasGroups[groupIndex].push(consulta);
            });

            this.especialidades.push('Generalista');
            JSON.parse(this.proposal.produtos).forEach(prod => {
                    if (!this.especialidades.includes(prod.especialidade)) {
                        this.especialidades.push(prod.especialidade);
                    }
                }
            );
            console.log('especialidades: ', this.especialidades);

            this.medicos = await this.doctorsService.getDoctors(this.clinica, this.especialidades);
            console.log('Medicos: ', this.medicos);
        }
    }

    chooseClinicImg(clinicName: string): string {
        if (clinicName === 'Clínica Douro') {
            this.clinicaTexto = 'A AngelSmile Douro está localizada numa das regiões mais bonitas do nosso país,' +
                ' aliando a tranquilidade de São Félix da Marinha com a beleza e história da região do Porto.' +
                ' Conjugando o conforto com a modernidade, esta clínica dispõe de espaços amplos que privilegiam o bem-estar do paciente.';
            this.clinicaCardImage1 = 'assets/images/cadeira-de-dentista.png';
            this.clinicaCardText1 = 'Espaço amplo e confortável';
            this.clinicaCardImage2 = 'assets/images/tooth-3.svg';
            this.clinicaCardText2 = 'Todas as especialidades';
            this.clinicaCardImage3 = 'assets/images/carro-ecologico.svg';
            this.clinicaCardText3 = 'Facilidade de estacionamento';
            return 'assets/images/clinica-douro.jpg';

        } else if (clinicName === 'Clinica Minho') {
            this.clinicaTexto = 'Situada no coração da cidade de Barcelos, a AngelSmile Minho está dotada com 5 consultórios' +
                ' preparados para todas as especialidades, salas de apoio ao paciente, sala de multimédia e Laboratório. ' +
                'Todo o percurso do paciente é pensado ao pormenor para garantir uma experiência de transformação personalizada. ';
            this.clinicaCardImage1 = 'assets/images/implant.png';
            this.clinicaCardText1 = 'Laboratório interno de Próteses Dentárias';
            this.clinicaCardImage2 = 'assets/images/screen.png';
            this.clinicaCardText2 = 'Sala de Multimédia';
            this.clinicaCardImage3 = 'assets/images/cadeira-de-dentista.png';
            this.clinicaCardText3 = '5 consultórios equipados com sistemas moderno';
            return 'assets/images/clinica-minho.jpg';
        } else {
            this.clinicaTexto = 'A AngelSmile Tejo é bem no centro da capital, na zona do Saldanha. A clínica é composta por ' +
                'pátios interiores, que trazem a privacidade necessária para todo o processo de transformação.' +
                ' O ambiente criado vai propiciar a serenidade e impedir que o medo do dentista entre no consultório.';
            this.clinicaCardImage1 = 'assets/images/trabalho.svg';
            this.clinicaCardText1 = 'No coração de Lisboa';
            this.clinicaCardImage2 = 'assets/images/cadeira-de-dentista.png';
            this.clinicaCardText2 = '10 consultórios com tecnologia de última geração';
            this.clinicaCardImage3 = 'assets/images/sofa.svg';
            this.clinicaCardText3 = 'Ambiente moderno e intimista';
            return 'assets/images/clinica-minho.jpg';

        }
    }

    navigateFragment(fragment: string): void {
        this.router.navigate([this.proposalId, 'proposal'], {fragment}).then();
    }

    abrirProposta(): void {
        window.open(this.proposal.pdf, '_blank');
    }

    showCasoVideo(video: string): void {
        this.dialog.open(DialogCasoVideoComponent, {
            width: '90%',
            maxHeight: '90%',
            data: {video}
        });
    }
}

@Component({
    selector: 'app-dialog-caso-video',
    template: '<div style="position: relative; display: block; width: 100%; padding-bottom: 56.25%">' + // 56.25% = 16/9 ratio
        '<iframe width="100%" height="100%" style="position: absolute;" [src]="safeSrc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
        '</div>'
})


class DialogCasoVideoComponent {
    safeSrc: SafeResourceUrl;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { video: string },
        private sanitizer: DomSanitizer
    ) {
        this.safeSrc = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + data.video);
    }
}

export {
    PropostaComponent,
    DialogCasoVideoComponent
};
