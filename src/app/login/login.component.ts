import { Component, OnInit } from '@angular/core';
import {Proposal, ProposalsService} from '../shared/proposals.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  proposal: Proposal;
  nif: string;

  private proposalId: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private proposalsService: ProposalsService,
    private matSnackBar: MatSnackBar,
  ) { }

  async ngOnInit(): Promise<void> {
    this.proposalId = this.route.snapshot.paramMap.get('id');
    this.proposal = await this.proposalsService.setProposal(this.proposalId);
  }

  isNifValid(): boolean {
    return this.nif && this.nif.length === 9;
  }

  handleNifSubmit(): void {
    if (!this.isNifValid()) {
      this.matSnackBar.open('Nif inválido. Tente novamente', 'ERRO', {
        verticalPosition: 'top',
        duration: 2000
      });
    } else if (this.proposalsService.authenticate(this.nif)) {
      this.router.navigate([this.proposalId, 'proposal']);
    } else {
      this.matSnackBar.open('Nif incorrecto. Tente novamente', 'ERRO', {
        verticalPosition: 'top',
        duration: 2000
      });
    }
  }

}
