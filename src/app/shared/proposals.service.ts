import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

interface Proposal {
  pacienteNome: string;
  nif: string;
  produtos: string;
  clinicaNome: string;
  cadeira: number;
  valor: number;
  descontoTotal: number;
  useremail: string;
  usertele: string;
  usernome: string;
  userfoto: string;
  antes: string;
  depois: string;
  consultas: string;
  pdf: string;
  metodos: string;
  mensalidade: string;
  taeg: string;
}

@Injectable({
  providedIn: 'root'
})
class ProposalsService {

  private proposal: Proposal;
  private authenticated = false;

  constructor(private httpClient: HttpClient) { }

  async setProposal(id: string): Promise<Proposal> {
    this.proposal = await this.retrieveProposal(id);
    return this.proposal;
  }

  getProposal(): Proposal {
    return this.proposal;
  }

  authenticate(nif): boolean {
    this.authenticated = nif === this.proposal.nif;
    return this.authenticated;
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  private retrieveProposal(id: string): Promise<Proposal> {
    return new Promise<Proposal>((resolve, reject) =>
      this.httpClient.get(environment.apiUrl + 'proposals/' + id)
        .subscribe(response => resolve((response as any).data[0] as Proposal), reject)
    );
  }

}

export {
  Proposal,
  ProposalsService
};
