import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

interface Doctor {
  id: number;
  nome: string;
  clinica: string;
  deleted: string;
  especialidade: string;
  fotografia: string;
}

@Injectable({
  providedIn: 'root'
})
class DoctorsService {

  constructor(private httpClient: HttpClient) { }

  getDoctors(clinic: string, specialities: any[]): Promise<Doctor[]> {
    return new Promise((resolve, reject) => {
      const headers = {
        Authorization: 'Bearer my-token',
        clinica: clinic,
        especialidades: JSON.stringify(specialities)
      };
      console.log('HERE', headers);
      this.httpClient.get(environment.apiUrl + 'doctors/clinic', { headers })
        .subscribe((doctorsResponse: any) => {
          let doctors = doctorsResponse.data;
          if (doctors.length > 7) {
            doctors.splice(0, doctors.length - 7);
          }
          doctors = doctors.map(doctor => ({
            ...doctor,
            fotografia: doctor.fotografia || 'assets/images/doctor-alt.png'
          }));
          resolve(doctors as Doctor[]);
        }, reject);
    });
  }
}

export {
  Doctor,
  DoctorsService
};
