FROM node:12.13.1-alpine AS build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
RUN npm run build --prod


# Stage 1: serve app with nginx server
FROM nginx:latest
COPY --from=build-step /app/dist/propostas  /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
